#include "rainSensor.h"

// Constructors/Destructors
rainSensor::rainSensor()
  {
  }

rainSensor::~rainSensor()
  {
  }

// Methods
void rainSensor::measureRainValue(bool rain)
  {
    rainValue = rain;
  }

bool rainSensor::requestRainValue()
  {
    return rainValue;
  }




