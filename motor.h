
#ifndef MOTOR_H
#define MOTOR_H

#include <iostream>
using namespace std;

class motor
  {
  private:
    //Attributes
    bool moving = false;
    bool state = false;

  public:
    // Constructors/Destructors
    motor();
    virtual ~motor();

    //Method
    bool open();
    bool close();
    bool giveState();
  };

#endif // MOTOR_H
