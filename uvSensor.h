
#ifndef UVSENSOR_H
#define UVSENSOR_H

#include <iostream>
using namespace std;

class uvSensor
  {
  public:
    // Constructors/Destructors
    uvSensor();
    virtual ~uvSensor();

    // Methods
    void measureUvValue(bool uv);
    bool requestUvValue();

  private:
    //Attributes
    bool uvValue;
  };

#endif // UVSENSOR_H
