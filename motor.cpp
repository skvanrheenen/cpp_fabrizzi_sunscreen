#include "motor.h"

// Constructors/Destructors
motor::motor()
  {
  }

motor::~motor()
  {
  }

// Methods
bool motor::open()
  {
    if (moving == false && state == false)
      {
        moving = true;
        state = true;
      }
    moving = false;
    return state;
  }

bool motor::close()
  {
    if (moving == false && state == true)
      {
        moving = true;
        state = false;
      }
    moving = false;
    return state;
  }

bool motor::giveState()
  {
    return state;
  }



