
#ifndef WINDSENSOR_H
#define WINDSENSOR_H

#include <iostream>
using namespace std;

class windSensor
  {
  public:
    // Constructors/Destructors
    windSensor();
    virtual ~windSensor();

    // Methods
    void measureWindValue(int wind);
    int requestWindValue();

  private:
    // Attributes
    int windValue;
  };

#endif // WINDSENSOR_H
