
#ifndef TEMPSENSOR_H
#define TEMPSENSOR_H

#include <iostream>
using namespace std;

class tempSensor
  {
  public:
    // Constructors/Destructors
    tempSensor();
    virtual ~tempSensor();

    // Methods
    void measureTempValue(int measuredTemp);
    int requestTempValue();

    private:
    int tempValue;
  };

#endif // TEMPSENSOR_H
