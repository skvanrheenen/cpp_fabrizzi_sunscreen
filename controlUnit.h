
#ifndef CONTROLUNIT_H
#define CONTROLUNIT_H

#include <iostream>
using namespace std;

#include "rainSensor.h"
#include "uvSensor.h"
#include "tempSensor.h"
#include "motor.h"
#include "luxSensor.h"
#include "windsensor.h"

class controlUnit
  {
  private:
    //Attributes
    int roomTemp;
    bool requiredState = false;
    int windThreshold = 3;
    int luxThreshold = 75000;
    bool uvThreshold = false;
    bool rainThreshold = false;

    //Methods
    void initSensorAttributes();
    bool checkState();
    void operateMotor();

    //Objects
    motor Motor;
    uvSensor UvSensor;
    rainSensor RainSensor;
    luxSensor LuxSensor;
    windSensor WindSensor;
    tempSensor TempSensor;

  public:
    // Constructors/Destructors
    controlUnit();
    virtual ~controlUnit();

    //Method
    void run();
    int setTemp(int temp);
    bool determineRequiredState(); //Made public for testing
  };

#endif // CONTROLUNIT_H
