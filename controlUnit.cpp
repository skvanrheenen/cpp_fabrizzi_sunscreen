#include "controlUnit.h"

// Constructors/Destructors
controlUnit::controlUnit()
  {
  }

controlUnit::~controlUnit()
  {
  }

// Methods
void controlUnit::initSensorAttributes()
  {
    int wind = 3;
    int lux = 75000;
    bool uv = false;
    bool rain = false;
  }

int controlUnit::setTemp(int temp)
  {
    roomTemp = temp;
    return roomTemp;
  }

bool controlUnit::checkState()
  {
    return Motor.giveState();
  }

bool controlUnit::determineRequiredState()
  {
    if(UvSensor.requestUvValue() && !RainSensor.requestRainValue() && (WindSensor.requestWindValue() <= windThreshold) && (LuxSensor.requestLuxValue() >= luxThreshold) && (TempSensor.requestTempValue() > roomTemp))
      {
        requiredState = true;
      }
    else
      {
        requiredState = false;
      }
    return requiredState;
  }

void controlUnit::operateMotor()
  {
    if(requiredState)
      {
        Motor.open();
      }
    else
      {
        Motor.close();
      }
  }

void controlUnit::run()
  {
    initSensorAttributes();
    checkState();
    determineRequiredState();
    operateMotor();
  }
