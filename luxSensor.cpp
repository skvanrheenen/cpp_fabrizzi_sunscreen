#include "luxSensor.h"

// Constructors/Destructors
luxSensor::luxSensor()
  {
  }

luxSensor::~luxSensor()
  {
  }

// Methods
void luxSensor::measureLuxValue(int lux)
  {
    luxValue = lux;
  }

int luxSensor::requestLuxValue()
  {
    return luxValue;
  }

