
#ifndef LUXSENSOR_H
#define LUXSENSOR_H

#include <iostream>
using namespace std;

class luxSensor
  {
  public:
    // Constructors/Destructors
    luxSensor();
    virtual ~luxSensor();

    // Methods
    void measureLuxValue(int lux);
    int requestLuxValue();

  private:
    // Attributes
    int luxValue;
  };

#endif // LUXSENSOR_H
