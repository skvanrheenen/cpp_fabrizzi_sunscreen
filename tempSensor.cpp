#include "tempSensor.h"

// Constructors/Destructors
tempSensor::tempSensor()
  {
  }

tempSensor::~tempSensor()
  {
  }

// Methods
void tempSensor::measureTempValue(int measuredTemp)
  {
    tempValue = measuredTemp;
  }

int tempSensor::requestTempValue()
  {
    return tempValue;
  }

