#include "testing.h"

// Constructors/Destructors
Testing::Testing()
  {
  }

Testing::~Testing()
  {
  }

// Methods
bool Testing::uvTest()
  {
    //UV sensor returns "true" when sun is shining and returns "false" when sun is not shining
    UvSensorTest.measureUvValue(true); // sun shines
    if(UvSensorTest.requestUvValue())
    {
        UvTestSunShines = true;
    }
    else
    {
        UvTestSunShines = false;
    }

    UvSensorTest.measureUvValue(false); // sun doesn't shine
    if(UvSensorTest.requestUvValue())
    {
        UvTestSunDoesntShine = false;
    }
    else
    {
        UvTestSunDoesntShine = true;
    }

    if(UvTestSunShines && UvTestSunDoesntShine)
    {
        UvTest = true; // Sensor give right value when the sun shines and when the sun doesn't shine
        cout<<"uvTest passed"<<endl;
    }
    else
    {
        UvTest = false;
        cout<<"uvTest failed"<<endl;
    }
    return UvTest;
  }

bool Testing::rainTest()
  {
    //Rain sensor returns "true" when it rains returns "false" when it's dry
    RainSensorTest.measureRainValue(true); // It does rain
    if(RainSensorTest.requestRainValue())
    {
        RainTestRain = true;
    }
    else
    {
        RainTestRain = false;
    }

    RainSensorTest.measureRainValue(false); // It doesn't rain
    if(!RainSensorTest.requestRainValue())
    {
        RainTestNoRain = true;
    }
    else
    {
        RainTestNoRain = false;
    }

    if(RainTestRain && RainTestNoRain)
    {
        RainTest = true; // Sensor give right value when it rains and when its dry
        cout<<"rainTest passed"<<endl;
    }
    else
    {
        RainTest = false;
        cout<<"rainTest failed"<<endl;
    }
    return RainTest;
  }

bool Testing::windTest()
  {
    //Wind sensor returns WindInput when put into measureWindValue()
    WindSensorTest.measureWindValue(WindInput);
    if(WindSensorTest.requestWindValue() == WindInput)
    {
        WindTest = true; //When input == output
        cout<<"windTest passed"<<endl;
    }
    else
    {
        WindTest = false; //When input != output
        cout<<"windTest failed"<<endl;
    }
    return WindTest;
  }

bool Testing::luxTest()
  {
    //Wind sensor returns WindInput when put into measureWindValue()
    LuxSensorTest.measureLuxValue(LuxInput);
    if(LuxSensorTest.requestLuxValue() == LuxInput)
    {
        LuxTest = true; //When input == output
        cout<<"luxTest passed"<<endl;
    }
    else
    {
        LuxTest = false; //When input != output
        cout<<"luxTest failed"<<endl;
    }
    return LuxTest;
  }

bool Testing::tempTest()
  {
    //Temp sensor returns TempInput when put into measureTempValue()
    TempSensorTest.measureTempValue(TempInput);
    if(TempSensorTest.requestTempValue() == TempInput)
    {
        TempTest = true; //When input == output
        cout<<"tempTest passed"<<endl;
    }
    else
    {
        TempTest = false; //When input != output
        cout<<"tempTest failed"<<endl;
    }
    return TempTest;
  }

bool Testing::motorTest()
  {
    //MotorTest.open() returns state = true
    //state = true; //activate of deactivate to test different situations
    state = false;  //activate of deactivate to test different situations
    if(testMotor.open() == true)
    {
        MotorTestOpen = true;
    }
    else
    {
        MotorTestOpen = false;
    }
    //state = true; //activate of deactivate to test different situations
    state = false;  //activate of deactivate to test different situations
    if(testMotor.close() == false)
    {
        MotorTestClosed = true;
    }
    else
    {
        MotorTestClosed = false;
    }

    if(MotorTestOpen && MotorTestClosed)
    {
        MotorTest = true;
        cout<<"motorTest passed"<<endl;
    }
    else
    {
        MotorTest = false;
        cout<<"motorTest failed"<<endl;
    }
    return MotorTest;
  }

bool Testing::controlUnitTest()
{
    if(testControlUnit.setTemp(temp) == temp)
    {
        setTempTest = true; //When output == input
    }
    else
    {
        setTempTest = false; //When output != input
    }

    /* For allowed to open the following values are entered in the method ControlUnitTest.determineRequiredState() based on requirements:
     * UvSensorTest.requestUvValue() = true, meaning sun is shining
     * RainSensorTest.requestRainValue() = false, meaning it is not raining
     * WindSensorTest.requestWindValue() = 2, meaning wind is lower than threshold --> allowed to open
     * LuxSensorTest.requestLuxValue() = 80000, meaning lux is higher than threshold --> allowed to open
     * TempSensorTest.requestTempValue() = 30, meaning temp is higher than threshold --> allowed to open
    */

    //ControlUnitTest.determineRequiredState() - required state open;
    if(true && !false && (2 <= windThresholdTest) && (80000 >= luxThresholdTest) && (30 > temp))
      {
        requiredStateOpen = true; //meaning open
      }
    else
      {
        requiredStateOpen = false; //meaning closed
      }

    /* For not allowed to open the following values are entered in the method ControlUnitTest.determineRequiredState() based on requirements:
     * UvSensorTest.requestUvValue() = false, meaning sun is shining
     * RainSensorTest.requestRainValue() = false, meaning it is not raining
     * WindSensorTest.requestWindValue() = 2, meaning wind is lower than threshold --> allowed to open
     * LuxSensorTest.requestLuxValue() = 80000, meaning lux is higher than threshold --> allowed to open
     * TempSensorTest.requestTempValue() = 30, meaning temp is higher than threshold --> allowed to open
    */

    //ControlUnitTest.determineRequiredState() - required state closed;
    if(false && !false && (2 <= windThresholdTest) && (80000 >= luxThresholdTest) && (30 > temp))
      {
        requiredStateClosed = false; //meaning open
      }
    else
      {
        requiredStateClosed = true; //meaning closed
      }

    //testControlUnit.operateMotor() with required state closed
    if(requiredStateClosed)
      {
        testState = true;
      }
    else
      {
        testState = false;
      }

    if(setTempTest == true && requiredStateOpen == true && requiredStateClosed == true && testState == true)
    {
        ControlUnitTest = true;
        cout<<"ControlUnitTest passed"<<endl;
    }
    else
    {
        ControlUnitTest = false;
        cout<<"ControlUnitTest failed"<<endl;
    }
    return ControlUnitTest;
}

void Testing::runTests()
{
    if(run == true) //run test once
    {
       uvTest();
       rainTest();
       windTest();
       luxTest();
       tempTest();
       motorTest();
       controlUnitTest();
       run = false;
    }

}
