#include <iostream>
using namespace std;

#include "controlUnit.h"
#include "testing.h"

int main()
{
    //Objects
    controlUnit ControlUnit;
    Testing Testing;

    while(1)
      {
         ControlUnit.setTemp(30);
         ControlUnit.run();
         Testing.runTests();
      }
    return 0;
}
