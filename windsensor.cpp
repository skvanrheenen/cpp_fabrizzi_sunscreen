#include "windsensor.h"

// Constructors/Destructors
windSensor::windSensor()
  {
  }

windSensor::~windSensor()
  {
  }

//Methods
void windSensor::measureWindValue(int wind)
  {
    windValue = wind;
  }

int windSensor::requestWindValue()
  {
    return windValue;
  }
