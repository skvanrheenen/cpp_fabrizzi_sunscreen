#ifndef TESTING_H
#define TESTING_H

#include <iostream>
using namespace std;

#include "rainSensor.h"
#include "uvSensor.h"
#include "tempSensor.h"
#include "motor.h"
#include "luxSensor.h"
#include "windsensor.h"
#include "controlUnit.h"

class Testing
{
public:
  Testing();
  virtual ~Testing();

  // Methods
  void runTests();

private:
  // Attributes
  bool UvTest;
  bool UvTestSunShines;
  bool UvTestSunDoesntShine;
  bool RainTest;
  bool RainTestRain;
  bool RainTestNoRain;
  bool WindTest;
  int WindInput = 30;
  bool LuxTest;
  int LuxInput = 75000;
  bool TempTest;
  int TempInput = 5;
  bool MotorTest;
  bool MotorTestOpen;
  bool MotorTestClosed;
  bool setTempTest;
  int temp = 26;
  int windThresholdTest = 3;
  int luxThresholdTest = 75000;
  bool requiredStateOpen;
  bool requiredStateClosed;
  bool ControlUnitTest;
  bool testState;
  bool state;
  bool run = true;

  // Objects
  uvSensor UvSensorTest;
  rainSensor RainSensorTest;
  windSensor WindSensorTest;
  luxSensor LuxSensorTest;
  tempSensor TempSensorTest;
  motor testMotor;
  controlUnit testControlUnit;

  // Methods
  bool uvTest();
  bool rainTest();
  bool windTest();
  bool luxTest();
  bool tempTest();
  bool motorTest();
  bool controlUnitTest();


};

#endif // TESTING_H
