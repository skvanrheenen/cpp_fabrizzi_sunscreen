
#ifndef RAINSENSOR_H
#define RAINSENSOR_H

#include <iostream>
using namespace std;

class rainSensor
  {
  public:
    // Constructors/Destructors
    rainSensor();
    virtual ~rainSensor();

    // Methods
    void measureRainValue(bool rain);
    bool requestRainValue();

  private:
    //Attributes
    bool rainValue;
  };

#endif // RAINSENSOR_H
