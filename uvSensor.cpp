#include "uvSensor.h"

// Constructors/Destructors
uvSensor::uvSensor()
  {
  }

uvSensor::~uvSensor()
  {
  }

//Methods
void uvSensor::measureUvValue(bool uv)
  {
    uvValue = uv;
  }

bool uvSensor::requestUvValue()
  {
    return uvValue;
  }


